import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.multioutput import MultiOutputRegressor
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe
from sklearn.metrics import accuracy_score
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor

# xgboost
import xgboost as xgb
from xgboost import XGBClassifier
print("xgboost", xgb.__version__)

#Data loading 
df_testX=pd.read_csv('algos-ds/data/x_test_pf4T2aK.csv')
print(f"the dataframe consists of {df_testX.shape[0]} entries over {df_testX.shape[1]} series")
print(df_testX.head())
print("-" * 55)
df_trainX=pd.read_csv('algos-ds/data/x_train_ACFqOMF.csv')
print(f"the dataframe consists of {df_trainX.shape[0]} entries over {df_trainX.shape[1]} series")
print(df_trainX.head())
print("-" * 55)
df_trainY=pd.read_csv('algos-ds/data/y_train_HNMbC27.csv',index_col=0)
print(f"the dataframe consists of {df_trainY.shape[0]} entries over {df_trainY.shape[1]} series")
print(df_trainY.head())

print("-" * 80)
print("-" * 80)
print("-" * 80)


# fit model no training data
#model = XGBClassifier()
#model.fit(df_trainX, df_trainY)
#print(model)
#print(df_trainY.describe())
#print(df_trainX.feature_names)


X=df_trainX
y=df_trainY
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

xg_reg = MultiOutputRegressor(xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.8, subsample=0.7,eta=0.1,
                max_depth = 5, alpha = 10, n_estimators = 100)).fit(X_train, y_train)

print(np.mean((xg_reg.predict(X_test) - y_test)**2, axis=0))

#data_dmatrix = xgb.DMatrix(data=X,label=y)
preds = xg_reg.predict(X_test)
rmse = np.sqrt(mean_squared_error(y_test, preds))
print("RMSE: %f" % (rmse))

outp=xg_reg.predict(df_testX)
np.savetxt('output.csv',outp)


########################
########################
########################
########################### SVR ###########################
########################
########################
########################

regressor=MultiOutputRegressor(SVR(kernel='rbf'))
regressor.fit(X_train,y_train)
preds=regressor.predict(X_test)
rmse = np.sqrt(mean_squared_error(y_test, preds))
print("RMSE: %f" % (rmse))


outp=regressor.predict(df_testX)
np.savetxt('outsvr.csv',outp,delimiter=' ')




########################
########################
########################
########################### RandomForestRegressor ###########################
########################
########################
########################


regressor=MultiOutputRegressor(RandomForestRegressor(n_estimators=10,random_state=0))
regressor.fit(X_train,y_train)
preds=regressor.predict(X_test)
rmse = np.sqrt(mean_squared_error(y_test, preds))
print("RMSE: %f" % (rmse))

outp=regressor.predict(df_testX)
np.savetxt('outRF.csv',outp,delimiter='')
