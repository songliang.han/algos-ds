import hyperopt
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
import warnings
warnings.filterwarnings('ignore')
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
from xgboost import XGBRegressor
from sklearn.model_selection import GridSearchCV
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

df_testX=pd.read_csv('/Users/home/Desktop/algos-ds/data/x_test_pf4T2aK.csv')
print(f"the dataframe consists of {df_testX.shape[0]} entries over {df_testX.shape[1]} series")


#print(df_testX.head())
#print("-" * 55)
df_trainX=pd.read_csv('/Users/home/Desktop/algos-ds/data/x_train_ACFqOMF.csv')
print(f"the dataframe consists of {df_trainX.shape[0]} entries over {df_trainX.shape[1]} series")

#print(df_trainX.head())
#print("-" * 55)
df_trainY=pd.read_csv('/Users/home/Desktop/algos-ds/data/y_train_HNMbC27.csv',index_col=0)
print(f"the dataframe consists of {df_trainY.shape[0]} entries over {df_trainY.shape[1]} series")
#print(df_trainY.head())

# X=df_trainX
# y=df_trainY.iloc[:,-5]

# X_train, X_test, y_train, y_test = train_test_split(
#      X, y, test_size=0.33, random_state=42)

space={'max_depth': hp.quniform("max_depth", 3, 18, 1),
        'gamma': hp.uniform ('gamma', 1,9),
        'reg_alpha' : hp.quniform('reg_alpha', 40,180,1),
        'reg_lambda' : hp.uniform('reg_lambda', 0,1),
        'colsample_bytree' : hp.uniform('colsample_bytree', 0.5,1),
        'min_child_weight' : hp.quniform('min_child_weight', 0, 10, 1),
        'n_estimators': 180
    }


# Regression: 
def hyperparameter_tuning(space):
    model=xgb.XGBRegressor(n_estimators =space['n_estimators'], max_depth = int(space['max_depth']), gamma = space['gamma'],
                         reg_alpha = int(space['reg_alpha']),min_child_weight=space['min_child_weight'],
                         colsample_bytree=space['colsample_bytree'])
    
    evaluation = [( X_train, y_train), ( X_test, y_test)]
    
    model.fit(X_train, y_train,
            eval_set=evaluation, eval_metric="rmse",
            early_stopping_rounds=2,verbose=False)

    pred = model.predict(X_test)
    mse= mean_squared_error(y_test, pred)
    print ("SCORE:", mse)
    #change the metric if you like
    return {'loss':mse, 'status': STATUS_OK, 'model': model}

predFin=pd.DataFrame()
for i in range(len(list(df_trainY.columns))):
    X=df_trainX
    y=df_trainY.iloc[:,i]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    trials = Trials()
    best = fmin(fn=hyperparameter_tuning,
            space=space,
            algo=tpe.suggest,
            max_evals=2,
            trials=trials)
    TheList=['max_depth','min_child_weight']
    for sub in TheList:
        if isinstance(best[sub], float):
            best[sub] = int(best[sub])
    
    # Instantiate the regressor: gbm
    gbm = xgb.XGBRegressor()
    gbm.set_params(**best)

    # Fit grid_mse to the data
    gbm.fit(X, y)
    preds = gbm.predict(df_testX)
    print(preds)
    s = pd.Series(preds)

    predFin=pd.concat([predFin, s], axis=1)

#
predFin.columns=df_trainY.columns
predFin.to_csv (r'/Users/home/Desktop/algos-ds/data/subtest4.csv', index = True)