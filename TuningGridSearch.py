import warnings
warnings.filterwarnings('ignore')
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
from xgboost import XGBRegressor
from sklearn.model_selection import GridSearchCV
import xgboost as xgb

#
#Data loading 
#

df_testX=pd.read_csv('/usr/users/gpupro/gpu_essaid/DS/data/x_test_pf4T2aK.csv')
print(f"the test dataframe consists of {df_testX.shape[0]} entries over {df_testX.shape[1]} series")


#print(df_testX.head())
#print("-" * 55)
df_trainX=pd.read_csv('/usr/users/gpupro/gpu_essaid/DS/data/x_train_ACFqOMF.csv')
print(f"the X_train dataframe consists of {df_trainX.shape[0]} entries over {df_trainX.shape[1]} series")

#print(df_trainX.head())
#print("-" * 55)
df_trainY=pd.read_csv('/usr/users/gpupro/gpu_essaid/DS/data/y_train_HNMbC27.csv',index_col=0)
print(f"the Y_train dataframe consists of {df_trainY.shape[0]} entries over {df_trainY.shape[1]} series")
#print(df_trainY.head())




y = df_trainY #.iloc[:, -1]
X = df_trainX
test = df_testX

#Parameter space
gbm_param_grid = { 
    'colsample_bytree': [0.7,0.6,0.8,0.9,1],
    'n_estimators': [1000,2000],
    'max_depth': [4,5,6,7],
    'eta': [0.1,0.01,0.05,0.2],
    'min_child_weight':[4,5,6,7],
    'subsample':[0.6,0.7,0.8,0.9],
    'gamma':[0,0.1,0.2],
    'reg-alpha':[0.001,0.1,1,5]

}

# Instantiate the regressor: gbm
gbm = xgb.XGBRegressor()

# Perform grid search: grid_mse
grid_mse = GridSearchCV(param_grid=gbm_param_grid, estimator=gbm, 
                        scoring='neg_mean_squared_error', cv=4, verbose=1)

# Fit grid_mse to the data with MultiOutputRegressor:
grid_result = MultiOutputRegressor(grid_mse).fit(X, y)
#Make predictions
preds=grid_result.predict(test)
# Print predictions
df_out = pd.DataFrame(preds, columns = list(df_trainY.columns))
# Write submissions
df_out.to_csv (r'/usr/users/gpupro/gpu_essaid/DS/data/subfinall1.csv', index = True, header=True)

