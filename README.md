Lien vers la video: https://clipchamp.com/watch/Ov7vYndrS9V


challenge data Bankers and markets

The goal of this project is to understand how financial markets react when central bankers deliver official speeches. The speeches were processed by a predefined BERT-style transformer. 

Bidirectional Encoder Representations from Transformers (BERT) is a transformer-based machine learning technique for natural language processing (NLP) pre-training developed by Google. BERT was pretrained on two tasks: language modelling (15% of tokens were masked and BERT was trained to predict them from context) and next sentence prediction (BERT was trained to predict if a chosen next sentence was probable or not given the first sentence). As a result of the training process, BERT learns contextual embeddings for words. After pretraining, which is computationally expensive, BERT can be finetuned with less resources on smaller datasets to optimize its performance on specific tasks.

![](https://image.jiqizhixin.com/uploads/editor/ca8797f2-55ab-4548-852a-b73f5efc3177/1544732034481.png)

So, after the processing, the dimension of our input is 768 and the dimension of our output is 39. This is a regression problem with multidimensional output.

We used 3 different methods, namely XGBoost, Support Vector Regerssion and Random Forest. 

| methods | RMSE |
| ------ | ------ |
| XGBoost | 19.24 |
| SVR | 29.19 |
| Random Forest |21.32|

From the mean squared error, XGBoost shows the best performance.

The idea of XGboost is to learn a new function f(x) to fit the residual of the last prediction. When we get k trees after training, we need to predict the score of a sample. In fact, according to the characteristics of this sample, there will be a corresponding leaf node in each tree, and each leaf node corresponds to a score. Finally, you only need to add up the scores corresponding to each tree to obtain the predicted value of the sample.

![](https://julyedu-img.oss-cn-beijing.aliyuncs.com/quesbase64153438657261833493.png)

XGBoost uses first-order and second-order partial derivatives, and the second-order derivative is conducive to faster and more accurate gradient descent. Using Taylor expansion to obtain the function as the second derivative form of the independent variable, the leaf splitting optimization calculation can be performed only by relying on the value of the input data without selecting the specific form of the loss function. In essence, the selection of the loss function is Separated from model algorithm optimization/parameter selection. This decoupling increases the applicability of XGBoost, making it choose the loss function on demand.


